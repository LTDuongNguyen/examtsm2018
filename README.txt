This repo is the final project for the Python course in February in TSM, Toulouse.
- Data folder has 3 csv files which are the database itself, and two dataframes of the probabilities of food and drink calculated in part 1 of the project.
- Documentation folder keeps the requirement of the project (a pptx file). 
- Code folder contains our work for the project: the Exploratory.py and Part2+3.py. The Exploratory.py has the answers for part 1 of the project. And part 2 and 3 are combined in the other python file.
- Results folder has two figure ploted based on the requirements in the part 1 of the project (bar plots of the total amount of sold foods (Figure_food) and drinks (Figure-drinks) over the five years).