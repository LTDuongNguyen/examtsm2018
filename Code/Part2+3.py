import random
import pandas as pd
import os

# Loading the 2 probability tables
path1 = os.path.abspath('./Data/Probabilities_drinks.csv')
pDrinks = pd.read_csv(path1, sep=',', names=["timed", "coffee", "frappucino", "milkshake", "soda", "tea", "water"], header=0)
path2 = os.path.abspath('./Data/Probabilities_food.csv')
pFood = pd.read_csv(path2, sep=',', names=["timef", "cookie", "muffin", "nothing", "pie", "sandwich"], header=0)
timeframe = pFood.timef
# Loading the database
outpath = os.path.abspath('./Data/Coffeebar_2013-2017.csv')
data = pd.read_csv(outpath, sep=";")

# Define a Customer class
class Customer(object):
    Custid = 0
    def __init__(self):
        self.ID = 'Customer' + str(Customer.Custid)
        Customer.Custid = Customer.Custid+1

# Define returning customer
class ReturningCust(Customer):
    def __init__(self):
        Customer.__init__(self)
        self.budget = 0
        self.time = random.randint(2, 10)

    def budget(self, price):
        if self.budget >= price:
            self.budget = self.budget - price
        else:
            raise Exception("Customer %s does not have enough money." % self.Id)
        return 1

    def timeslot(self):
        record = []
        for i in range(self.time):
            r = random.randint(1, len(data.TIME))
            t = data.TIME[r]
            record.append(t)
        return record

    def buy(self, probD, probF):
        d = []
        priced = []
        f = []
        pricef = []
        p = []
        budget = []
        for i in range(self.time):
            x = self.timeslot()[i]
            y = list(timeframe).index(x[-8:-3])
            pbD = probD.iloc[y]
            pbF = probF.iloc[y]
            pbDc = pbD.coffee
            pbDf = pbD.frappucino
            pbDm = pbD.milkshake
            pbDs = pbD.soda
            pbDt = pbD.tea
            r = random.uniform(0, 1)
            if r < pbDc:
                drink = "Coffee"
                price_drink = 3
            elif r < (pbDc + pbDf):
                drink = "Frappucino"
                price_drink = 4
            elif r < (pbDc + pbDf + pbDm):
                drink = "Milkshake"
                price_drink = 5
            elif r < (pbDc + pbDf + pbDm + pbDs):
                drink = "Soda"
                price_drink = 3
            elif r < (pbDc + pbDf + pbDm + pbDs + pbDt):
                drink = "Tea"
                price_drink = 3
            else:
                drink = "Water"
                price_drink = 2
            pbFc = pbF.cookie
            pbFm = pbF.muffin
            pbFn = pbF.nothing
            pbFp = pbF.pie
            pbFs = pbF.sandwich
            r1 = random.uniform(0, 1)
            if r1 < pbFc:
                food = "Cookie"
                price_food = 2
            elif r1 < (pbFc + pbFm):
                food = "Muffin"
                price_food = 3
            elif r1 < (pbFc + pbFm + pbFn):
                food = "Nothing"
                price_food = 0
            elif r1 < (pbFc + pbFm + pbFn + pbFp):
                food = "Pie"
                price_food = 3
            else:
                food = "Sandwich"
                price_food = 5
            price = price_drink + price_food
            if self.budget >= price:
                self.budget = self.budget - price
            else:
                self.budget = "The budget is lower than the total price."
            d.append(drink)
            priced.append(price_drink)
            f.append(food)
            pricef.append(price_food)
            p.append(price)
            budget.append(self.budget)
        return {'Customer': self.ID, 'Time': (self.timeslot()), 'Drink': d, 'PriceD': priced, 'Food': f,
                'PriceF': pricef, 'Total': p, 'Budget remain': budget}


class OnetimeCust(Customer):
    def __init__(self):
        Customer.__init__(self)
        self.budget = 0

    def timeslot(self):
        rindex = random.randint(1, len(data['TIME']))
        timing = data['TIME'][rindex]
        return timing

    def buy(self, probD, probF):
        x = self.timeslot()
        y = list(timeframe).index(x[-8:-3])
        pbD = probD.iloc[y]
        pbF = probF.iloc[y]
        pbDc = pbD.coffee
        pbDf = pbD.frappucino
        pbDm = pbD.milkshake
        pbDs = pbD.soda
        pbDt = pbD.tea
        pbDw = pbD.water
        r = random.uniform(0, 1)
        if r < pbDc:
            drink = "Coffee"
            price_drink = 3
        elif r < (pbDc + pbDf):
            drink = "Frappucino"
            price_drink = 4
        elif r < (pbDc + pbDf + pbDm):
            drink = "Milkshake"
            price_drink = 5
        elif r < (pbDc + pbDf + pbDm + pbDs):
            drink = "Soda"
            price_drink = 3
        elif r < (pbDc + pbDf + pbDm + pbDs + pbDt):
            drink = "Tea"
            price_drink = 3
        else:
            drink = "Water"
            price_drink = 2
        pbFc = pbF.cookie
        pbFm = pbF.muffin
        pbFn = pbF.nothing
        pbFp = pbF.pie
        pbFs = pbF.sandwich
            #  The price for drinks is milkshake = 5, frappucino = 4, water = 2 and the rest 3.
        r1 = random.uniform(0, 1)
        if r1 < pbFc:
            food = "Cookie"
            price_food = 2
        elif r1 < (pbFc + pbFm):
            food = "Muffin"
            price_food = 3
        elif r1 < (pbFc + pbFm + pbFn):
            food = "Nothing"
            price_food = 0
        elif r1 < (pbFc + pbFm + pbFn + pbFp):
            food = "Pie"
            price_food = 3
        else:
            food = "Sandwich"
            price_food = 5
        price = price_drink + price_food
        remain = self.budget - price
        return {'Customer': self.ID, 'Time':(self.timeslot()), 'Drink': drink, 'PriceD': price_drink, 'Food': food,'PriceF': price_food, 'Total': price, 'Budget': self.budget, 'Budget remain': remain}


class RegOneTimeCust(OnetimeCust):
    def __init__(self):
        OnetimeCust.__init__(self)
        self.budget = 100


class TripAdvisorCust(OnetimeCust):
    def __init__(self):
        OnetimeCust.__init__(self)
        self.tips = random.randint(1, 10)
        self.budget = 100


class RegReturningCust(ReturningCust):
    def __init__(self):
        ReturningCust.__init__(self)
        self.budget = 250


class Hipsters(ReturningCust):
    def __init__(self):
        ReturningCust.__init__(self)
        self.budget = 500


# Testing the classes created above
c1 = RegOneTimeCust()
c2 = TripAdvisorCust()
c3 = Hipsters()
c4 = RegReturningCust()
print(c1.ID)
print(c2.ID)
print(c3.ID)
print(c4.ID)
print(c1.budget)
print(c2.budget)
print(c3.budget)
print(c4.budget)

# One time customer
One1 = c1.buy(pDrinks, pFood)
OneIndex = [1]
o1 = pd.DataFrame(data=One1, index=OneIndex,
                  columns=['Customer', 'Time', 'Drink', 'PriceD',
                           'Food', 'PriceF', 'Total', 'Budget', 'Budget remain'])
print(o1)

# TripAdvisor
Trip1 = c2.buy(pDrinks, pFood)
TripIndex = [10]
t1 = pd.DataFrame(data=Trip1, index=TripIndex,
                  columns=['Customer', 'Time', 'Drink', 'PriceD',
                           'Food', 'PriceF', 'Total', 'Budget', 'Budget remain'])
print(t1)

# Hipster
Hip1 = c3.buy(pDrinks, pFood)
h1 = pd.DataFrame(data=Hip1,
                  columns=['Customer', 'Time', 'Drink', 'PriceD',
                           'Food', 'PriceF', 'Total', 'Budget remain'])
print(h1)

# Returning customer
Return1 = c4.buy(pDrinks, pFood)
r1 = pd.DataFrame(data=Return1,
                  columns=['Customer', 'Time', 'Drink', 'PriceD',
                           'Food', 'PriceF', 'Total', 'Budget remain'])
print(r1)

# Simulation
ReturnCust = 10
HipsterCust = int(ReturnCust/3)
RemainCust = ReturnCust - HipsterCust
HipsterC = []
RemainC = []
for i in range(0, HipsterCust):
    j = Hipsters()
    l = j.buy(pDrinks, pFood)
    HipsterC.append(l)
for i in range(0, RemainCust):
    j= RegReturningCust()
    l = j.buy(pDrinks, pFood)
    RemainC.append(l)
ReturnC = HipsterC + RemainC
print(ReturnC)

OnetimeCust = 100
TripCust = int(OnetimeCust / 10)
RemainCCust = OnetimeCust - TripCust
TripC = []
RemainCC = []
for i in range(0, TripCust):
    j = TripAdvisorCust()
    l = j.buy(pDrinks, pFood)
    TripC.append(l)
for i in range(0, RemainCCust):
    j = RegOneTimeCust()
    l = j.buy(pDrinks, pFood)
    RemainCC.append(l)
OnetimeC = TripC + RemainCC
print(OnetimeC)

CustList = []
n = 1
while n < 100:
    r = random.uniform(0, 1)
    n = n+1
    if r <= 0.2:
        a = 'TRUE'
        while a == 'TRUE':
            newid = random.randint(0, len(ReturnC)-1)
            oldcustomer = ReturnC[newid]
            value = oldcustomer.values()
            test = oldcustomer['Budget remain'][(len(oldcustomer['Budget remain'])-1):][0]
            if test > 10:
                CustList.append(ReturnC[newid])
                a = 'FALSE'
            else:
                newid = random.randint(0, len(OnetimeC)-1)
                CustList.append(OnetimeC[newid])
                a = 'TRUE'
print(CustList)
