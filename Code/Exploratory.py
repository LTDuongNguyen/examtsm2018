import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# PART 1: DETERMINE PROBABILITIES
outpath = os.path.abspath('./Data/Coffeebar_2013-2017.csv')
data = pd.read_csv(outpath, sep=";")
# In order to understand more about the database, we will run the codes below
# This is our data about the coffee shop
print(data)
# This data has 312075 rows and 4 columns representing TIME, CUSTOMER, DRINKS and FOOD
print(data.columns)  # Getting column labels.
print(data.head(10))  # The first 10 rows of the database.
print(data.tail(10))  # Getting last 10 rows.

# Types of food sold at the coffee bar
print("Types of food: %s" % data.FOOD.unique())
# Types of drinks sold at the coffee bar
print("Types of drinks: %s" % data.DRINKS.unique())
# Total unique customers the bar had
print("The total number of unique customers the coffee bar had: %s" % len(data.CUSTOMER.unique()))

# Getting the number of each type of food sold in 5 years
print(data.groupby(data.FOOD).size())
# Making plot 1: a bar plot of the total amount of food sold foods\\\
object1 = ('cookie', 'muffin', 'pie', 'sandwich')
ypos1 = np.arange(len(object1))
performance1 = [31852, 31785, 32010, 68623]
plt.xticks(ypos1, object1)
plt.xlabel('Types of foods')
plt.ylabel('Amount of foods sold')
plt.title('Total amount of foods sold over 5 years')
plt.bar(ypos1, performance1, align='center')
plt.show()
# Getting the number of each type of drinks sold in 5 years
print(data.groupby(data.DRINKS).size())
# Making plot 2: a bar plot of the total amount of drinks sold
object2 = ('coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water')
ypos2 = np.arange(len(object2))
performance2 = [53833, 40436, 40752, 95583, 40556, 40915]
plt.xticks(ypos2, object2)
plt.xlabel('Types of drinks')
plt.ylabel('Amount of drinks sold')
plt.title('Total amount of drinks sold over 5 years')
plt.bar(ypos2, performance2, align='center')
plt.show()

# Determine the average that a customer buys a certain food or drink at any given time
# Convert the variables in column TIME to date format
data.TIME = pd.to_datetime(data.TIME)
# Using this code to convert the TIME to Hour:Minute format
df1 = data.TIME.dt.strftime("%H:%M")
print(df1)

# Grouping the data by TIME and CUSTOMER
# => we obtain the number of customer for a certain time period
customerpertime = data.groupby(df1)['CUSTOMER'].count()
print(customerpertime)
# Getting the type of drink at the specific time
drinkpertime = pd.concat([df1, data.DRINKS], axis=1, join_axes=[df1.index])
drinkpertime = pd.DataFrame(drinkpertime)
print(drinkpertime)
# Grouping the data by each type of DRINKS,
# we obtain the number of drinks sold in a specific time
x = pd.DataFrame(drinkpertime.groupby(['DRINKS', 'TIME']).size())
print(x)
# Based on the above results,
# we make a table having TIME as index,
# types of DRINKS of columns,
# and the number of observation as values
table1 = pd.pivot_table(x, index=["TIME"], columns=["DRINKS"])
table1 = pd.DataFrame(table1)
# Checking if the total drinks sold is equal to the number of customers
table1.sum(axis=1)
# Adding the total drinks sold column into the table
groupD = pd.concat([table1, customerpertime], axis=1)
groupD = pd.DataFrame(groupD)
# Rename the columns
groupD.columns = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water','total']
# Probability of each drink sold at every time slots
probD = groupD.apply(lambda x: x/groupD.total, axis=0)
print(probD)  #this shows the probability of every drinks at every time slots  

'''
These following codes is to save probD as a csv file named Probabilities_drinks
probD = pd.DataFrame(probD)
PD = probD.reset_index()
PD = pD.drop('total', 1)
PD = pd.DataFrame(PD)
PD.to_csv('Probabilities_drinks.csv', index=False, header=False)
'''

# Getting the type of food at the specific time
foodpertime = pd.concat([df1,data.FOOD],axis=1,join_axes=[df1.index])
foodpertime = pd.DataFrame(foodpertime)
foodpertime.fillna("nothing", inplace=True)
# Grouping the data by each type of FOOD,
# we obtain the number of food sold in a specific time
y = pd.DataFrame(foodpertime.groupby(['FOOD', 'TIME']).size())
print(y)
# Based on the above result,
# we make a table having TIME as rows,
# types of FOOD of columns,
# and the number of observation as values
groupF = pd.pivot_table(y, index=["TIME"], columns = ["FOOD"])
groupF = pd.DataFrame(groupF)
groupF = pd.concat([groupF, customerpertime], axis = 1)
# Rename the columns
groupF.columns = ['cookie','muffin', "nothing", 'pie', 'sandwich','total']
groupF = pd.DataFrame(groupF)
print(groupF)
# Giving the empty value by a value equal to 0
groupF.fillna(int("0"), inplace=True)
print(groupF)
#Probability of each food sold at every time slots
probF = groupF.apply(lambda x: x/groupF.total, axis=0)
print(probF)  #this shows the probability of every food at every time slots  

'''
These following codes is to save probF as a csv file named Probabilities_food.csv
probF = pd.DataFrame(probF)
PF = probF.reset_index()
PF = pF.drop('total', 1)
PF = pd.DataFrame(pF)
PF.to_csv('Probabilities_food.csv', index=False, header=False)
'''

# Extension
# For this following part, we want to be more specific and detailed about the probabality. 
# So these codes aim to give the probabilities for food and drinks at the time chosen by the user.
# Ask the user to input the hour that he/she wants to have the probabilities of the food and drinks sold.
time = input('Enter the time from 08:00 to 18:00 in format 00:00 (24 hours): ')
# Extracting the time column in two tables: groupD (drinks) and groupF (food)
t1 = groupD.index
t2 = groupF.index
# Conditions for the chosen time
if time in t1 and time in t2:
    # Select the row of the entered time
    f1 = groupD.loc[time]
    # Calculate the probability of each drink regarding the given time
    prob1 = '{0:.0%}'.format(f1.coffee / f1.total)
    prob2 = '{0:.0%}'.format(f1.frappucino / f1.total)
    prob4 = '{0:.0%}'.format(f1.soda / f1.total)
    prob5 = '{0:.0%}'.format(f1.tea / f1.total)
    prob6 = '{0:.0%}'.format(f1.water / f1.total)
    prob3 = '{0:.0%}'.format(f1.milkshake / f1.total)
    # Select the row of the entered time
    f2 = groupF.loc[time]
    # Calculate the probability of each food regarding the given time
    p1 = '{0:.0%}'.format(f2.cookie / f2.total)
    p2 = '{0:.0%}'.format(f2.muffin / f2.total)
    p4 = '{0:.0%}'.format(f2.pie / f2.total)
    p5 = '{0:.0%}'.format(f2.sandwich / f2.total)
    p3 = '{0:.0%}'.format(f2.nothing / f2.total)
    # Print the results 
    print("On average the probability of a customer at %s "
          "buying coffee, frappucino, milkshake, soda, tea, water is %s, %s, %s, %s, %s, %s "
          % (time, prob1, prob2, prob3, prob4, prob5, prob6))
    print("and for food %s cookies, %s muffins, %s nothing, %s pies, and %s sandwiches."
          % (p1, p2, p3, p4, p5))
else:
    # If the time entered is not in the working time range of the coffee bar, the following sentence will be displayed
    print("Time entered is not available. Please enter a different time between 08:00 to 18:00.")
